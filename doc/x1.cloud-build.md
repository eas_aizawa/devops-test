# (オプション) Cloud Build を利用した Docker image の自動ビルド

- 2010-09-07のハンズオンでは、Docker imageのビルドは、GCPのCloud Source Repositories と Cloud Build で自動実行していた
- これをGitFlowに沿って、Issue作成 → Merge Request(PullRequest) → CI → コードレビューというフローを体験できるようにGitLabを使うように変更したため、Cloud Buildは使わなくなった
- 参考までに、Cloud Build を利用する手順も残しておく

### Docker イメージのビルド

#### ソースコードリポジトリを作成し、Cloud Buildをセットアップ

- Cloud Source Repositories を有効にする
  - ブラウザのUIから作成してください
    - `devops-test` というリポジトリを作成
  - 料金情報
    - 最大 5 名のプロジェクト ユーザー1 が無料で使用できます
    - 無料枠には最大計 50 GB の無料ストレージと、1 か月あたり 50 GB の無料下りバイトが用意されています
- Cloud Shell にクローンしたコードに対して、ソースリポジトリを追加してプッシュする 

![](image/Source-Repositories-start.png)
![](image/Source-Repositories-new.png)
![](image/Source-Repositories-create.png)


```
$ cd ~/devops-test && \
  git config --global user.email "$(gcloud config get-value core/account)" && \
  export USERNAME=$(whoami) && \
  git config --global user.name $USERNAME && \
  git config --list && \
  git config credential.helper gcloud.sh && \
  export PROJECT=$(gcloud info --format='value(config.project)') && \
  git remote add google https://source.developers.google.com/p/$PROJECT/r/devops-test && \
  git add . && \
  git commit -m "commit on cloud shell" && \
  git push --all google
```

- Source Repositories にコード一式が追加される

![](image/Source-Repositories-pushed.png)


- Cloud Build を設定する
  - Gitリポジトリに `v.*` の形でタグがプッシュされると、 Container Registry にイメージがプッシュされるようにセット
    - Cloud Source Repositories の設定に「このリポジトリのビルドトリガーの管理 Cloud Build トリガー」があるのでここから設定
    - 名前: devops-test-by-tags
    - トリガーのタイプ: タグ
    - タグ（正規表現）: v.*
    - ビルド設定: Cloud Build 構成ファイル（yaml または json)
    - Cloud Build 構成ファイルの場所: /cloudbuild.yaml
  - 料金の情報
    - Cloud Buildは、$0.003/ビルド分。1 日あたり最初の 120 ビルド分は無料です。
      - Cloud Storage のストレージと下りネットワークに対してのみ課金（基本無料枠でいけるかと）

![](image/Source-Repositories-config.png)
![](image/cloudbuild-start.png)
![](image/cloudbuild-new.png)
![](image/cloudbuild-create.png)

